/*
Parser czyta napis (wyrażenie matematyczne w notacji infiksowej) i zwraca zbudowane wyrażenie w formie napisu (w notacji infiksowej)
*/

parse(N)  --> wyrazenie(N).

cyfra(Char) --> [Char], {char_type(Char, digit)}.

liczba([C|N]) --> cyfra(C), !, opc_liczba(N).
opc_liczba([C|N]) --> cyfra(C), !, opc_liczba(N).
opc_liczba([]) --> [].

wyrazenie(N) --> skladnik(S), !, opc_wyrazenie(A), { append(S,A,N) }.
opc_wyrazenie(N) --> "+", skladnik(S), !, opc_wyrazenie(A), { append([0'+|S],A,N) }.
opc_wyrazenie(N) --> "-", skladnik(S), !, opc_wyrazenie(A), { append([0'-|S],A,N) }.
opc_wyrazenie([]) --> [].

skladnik(N) --> czynnik(S), opc_skladnik(A), {append(S, A, N)}.
opc_skladnik(N) --> "*", czynnik(S), !, opc_skladnik(A), {append([0'*|S], A, N)}.
opc_skladnik(N) --> "/", czynnik(S), !, opc_skladnik(A), {append([0'/|S], A, N)}.
opc_skladnik([]) --> [].

czynnik(N) --> wyrazenie_proste(S), opc_czynnik(A), {append(S, A, N)}.
opc_czynnik(N) --> "^", wyrazenie_proste(S), !, opc_czynnik(A), {append([0'^|S], A, N)}.
opc_czynnik([]) --> [].

wyrazenie_proste(N) --> "(", wyrazenie(A), ")", {append([0'(|A], [0')], N)}.
wyrazenie_proste(N) --> liczba(N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t(In, Out) :-
	string_codes(In, A),
	parse(X,A,[]),
	string_codes(Out, X).

test(In):-
	string_codes(In, A),
	print(In),
	parse(A,A,[]),
	nl.
	
:- test("404").
:- test("2+5").
:- test("2-5").
:- test("2+2*5+3").
:- test("2+2/5+3").
:- test("125/5/5").
:- test("2^2").
:- test("2^2^3").
:- test("(2^2)^3").
:- test("(2+2)*5+3^(2+1)").
