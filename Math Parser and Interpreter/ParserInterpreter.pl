/*
Parser czyta napis (wyrażenie matematyczne w notacji infiksowej) i zwraca zbudowane wyrażenie w postaci drzewa
*/

parse(N) --> wyrazenie(N).

cyfra(C) --> [Char], {char_type(Char, digit), C is Char-"0"}.

liczba(num(N)) --> cyfra(C), !, opc_liczba(N,C).
opc_liczba(N,A) --> cyfra(C), !, {A1 is A*10+C}, opc_liczba(N,A1).
opc_liczba(N,N) --> [].

wyrazenie(N) --> skladnik(S), !, opc_wyrazenie(N,S).
opc_wyrazenie(N,A) --> "+", skladnik(S), !, opc_wyrazenie(N,op("+",A,S)).
opc_wyrazenie(N,A) --> "-", skladnik(S), !, opc_wyrazenie(N,op("-",A,S)).
opc_wyrazenie(N,N) --> [].

skladnik(N) --> czynnik(S), opc_skladnik(N,S).
opc_skladnik(N,A) --> "*", czynnik(S), !, opc_skladnik(N,op("*",A,S)).
opc_skladnik(N,A) --> "/", czynnik(S), !, opc_skladnik(N,op("/",A,S)).
opc_skladnik(N,N) --> [].

czynnik(N) --> wyrazenie_proste(S), opc_czynnik(N,S).
opc_czynnik(op("^",A,N), A) --> "^", wyrazenie_proste(S), !, opc_czynnik(N,S).
opc_czynnik(N,N) --> [].

wyrazenie_proste(N) --> "(", wyrazenie(N), ")".
wyrazenie_proste(N) --> liczba(N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%inter(In, Out):-
inter(op("+",A,B), Out):-
	inter(A, A1),
	inter(B, B1),
	Out is A1+B1.
inter(op("-",A,B), Out):-
	inter(A, A1),
	inter(B, B1),
	Out is A1-B1.
inter(op("*",A,B), Out):-
	inter(A, A1),
	inter(B, B1),
	Out is A1*B1.
inter(op("/",A,B), Out):-
	inter(A, A1),
	inter(B, B1),
	Out is A1/B1.
inter(op("^",A,B), Out):-
	inter(A, A1),
	inter(B, B1),
	Out is A1^B1.
inter(num(A), A).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t(In, Out) :-
	string_codes(In, A),
	parse(Out,A,[]).

test(Input, Parse, Inter):-
	write("Input: "), writeln(Input), 
	write("Parse: "), writeln(Parse),
	write("Inter: "), writeln(Inter),
	t(Input,Parse),
	inter(Parse, Inter),
	nl.
	
:- test("404", num(404), 404).
:- test("2+5", op("+",num(2),num(5)), 7).
:- test("2-5", op("-",num(2),num(5)), -3).
:- test("2+2*5+3", 
	op("+",op("+",num(2),op("*",num(2),num(5))),num(3)), 15).
:- test("2+2/5+3", 
	op("+",op("+",num(2),op("/",num(2),num(5))),num(3)), 5.4).
:- test("125/5/5", op("/",op("/",num(125),num(5)),num(5)), 5).
:- test("2^2", op("^",num(2),num(2)), 4).
:- test("2^2^3", op("^",num(2),op("^",num(2),num(3))), 256).
:- test("(2^2)^3", 
	op("^", op("^",num(2),num(2)),num(3)), 64).
:- test("(2+2)*5+3^(2+1)", 
	op("+",
		op("*",op("+",num(2),num(2)),num(5)),
		op("^",num(3),op("+",num(2),num(1)))
	), 47).
