Zestaw parserów i interpreterów zaimplementowanych w Prologu, które obsługują: 
 - pięć podstawowych operacji arytmetycznych: dodawanie, odejmowanie, mnożenie, dzielenie, oraz potęgowanie 
 - nawiasowanie: '(', ')'


| operatory | łączność | priorytet |
| --------  | -------- | --------- |
|     ^     |  w lewo  |     3     |
|    * /    |  w lewo  |     2     |
|    + -    |  w prawo |     1     |
