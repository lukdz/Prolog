/*
Interpreter wylicza i zwraca wartość napisu (wyrażenia matematycznego w notacji infiksowej)
*/

parse(N) --> wyrazenie(N).

cyfra(C) --> [Char], {char_type(Char, digit), C is Char-"0"}.

liczba(N) --> cyfra(C), !, opc_liczba(N,C).
opc_liczba(N,A) --> cyfra(C), !, {A1 is A*10+C}, opc_liczba(N,A1).
opc_liczba(N,N) --> [].

wyrazenie(N) --> skladnik(S), !, opc_wyrazenie(N,S).
opc_wyrazenie(N,A) --> "+", skladnik(S), !, {A1 is A+S}, opc_wyrazenie(N,A1).
opc_wyrazenie(N,A) --> "-", skladnik(S), !, {A1 is A-S}, opc_wyrazenie(N,A1).
opc_wyrazenie(N,N) --> [].

skladnik(N) --> czynnik(S), opc_skladnik(N,S).
opc_skladnik(N,A) --> "*", czynnik(S), !, {A1 is A*S}, opc_skladnik(N,A1).
opc_skladnik(N,A) --> "/", czynnik(S), !, {A1 is A/S}, opc_skladnik(N,A1).
opc_skladnik(N,N) --> [].

czynnik(N) --> wyrazenie_proste(S), opc_czynnik(P), {N is S^P}.
opc_czynnik(N) --> "^", wyrazenie_proste(S), !, opc_czynnik(P), {N is S^P}.
opc_czynnik(1) --> [].

wyrazenie_proste(N) --> "(", wyrazenie(N), ")".
wyrazenie_proste(N) --> liczba(N).

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t(In, Out) :-
	string_codes(In, Chars),
	parse(Out, Chars, _).

test(Data, Result):-
	print(Data),
	print(=),
	print(Result),
	t(Data,Result),
	nl.

:- test("404", 404).	
:- test("2+5", 7).
:- test("2-5", -3).
:- test("2+2*5+3", 15).
:- test("2+2/5+3", 5.4).
:- test("125/5/5", 5).
:- test("2^2", 4).
:- test("2^2^3", 256).
:- test("(2^2)^3", 64).
:- test("(2+2)*5+3^(2+1)", 47).
