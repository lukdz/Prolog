/*
Parser czyta napis (wyrażenie matematyczne w notacji infiksowej)
*/

parse --> wyrazenie.

cyfra --> [Char], {char_type(Char, digit)}.

liczba --> cyfra, !, opc_liczba.
opc_liczba --> cyfra, !, opc_liczba.
opc_liczba --> [].

wyrazenie --> skladnik, !, opc_wyrazenie.
opc_wyrazenie --> "+", skladnik, !, opc_wyrazenie.
opc_wyrazenie --> "-", skladnik, !, opc_wyrazenie.
opc_wyrazenie --> [].

skladnik --> czynnik, opc_skladnik.
opc_skladnik --> "*", czynnik, !, opc_skladnik.
opc_skladnik --> "/", czynnik, !, opc_skladnik.
opc_skladnik --> [].

czynnik --> wyrazenie_proste, opc_czynnik.
opc_czynnik --> "^", wyrazenie_proste, !, opc_czynnik.
opc_czynnik --> [].

wyrazenie_proste --> "(", wyrazenie, ")".
wyrazenie_proste --> liczba.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

t(In) :-
	string_codes(In, A),
	parse(A,[]).

test(In):-
	string_codes(In, A),
	print(In),
	parse(A,[]),
	nl.
	
:- test("404").
:- test("2+5").
:- test("2-5").
:- test("2+2*5+3").
:- test("2+2/5+3").
:- test("125/5/5").
:- test("2^2").
:- test("2^2^3").
:- test("(2^2)^3").
:- test("(2+2)*5+3^(2+1)").
